<?php
function Render($content)
{
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Bibliothèque de la ville d'Orange</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="view/css/style.css" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans">
	</head>
	<body>
		<div style="background-color:white;">
		<a href="index.php">Accueil</a>
		<a href="catalogue.php">Catalogue</a>
		<?php 
		if(isset($user)&&$user!=null){    
		    ?>
			<a href="profil.php">Profil</a>
		    <a href="deconnexion.php">Deconnexion</a>
			<?php 
		}else{
		    ?>	
			<a href="inscription.php">Inscription</a>
			<a href="connexion.php">Connexion</a>
			<?php 
		}
		?>
		</div>
		<?php
        try {
            require_once ($content);
        } catch (Exception $e) {
            echo("Page invalide");
        }
        ?>
	</body>
</html>
<?php
}
?>