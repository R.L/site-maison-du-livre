<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
	<meta charset="utf-8">
</head>
<body>
 
 <?php 
   session_start();

  if (isset($_SESSION['mail'])) {
      echo "Bienvenue " . $_SESSION['nom'] . " " . $_SESSION['prenom'] . " ! ";
      echo "<a href='./logout.php'>(Déconnexion)</a> ";
      echo "<a href='./profile.php'>(Modifier vos informations)</a>";

    ?> 

    <?php
  } else {
    ?>

    <form action="./index.php" method="post">
      <div class="container">
        <label><b>Adresse mail</b></label>
        <input type="text" placeholder="Entrer votre adresse mail" name="usrmail" required>

        <label><b>Mots de passe</b></label>
        <input type="password" placeholder="Entrer votre mot passe" name="usrpsw" required>

        <button type="submit">Connexion</button>
      </div>

     <div class="container" style="background-color:#f1f1f1">
       <span class="registration"><a href="./registration.php">S'inscrire maintenant</a> !</span>
     </div>
    </form>

  <?php 
  }
  ?>


</body>
</html>

<?php

  if (isset($_POST['usrmail'])) {

    $mail = $_POST['usrmail']; $password = $_POST['usrpsw'];
    $sql = new mysqli("localhost", "root", "", "librairie");
    $result = $sql -> query("SELECT * FROM user WHERE adresse_mail = '$mail'");

    if (mysqli_num_rows($result) == 0) {
        echo "Il n'existe aucun compte ayant cette adresse mail !";
        exit;
    }

    $data = $result -> fetch_assoc();
    
    if (strcmp($db_hash, $rehash) != 0) {
        echo "Mots de passe incorrect !";
        exit;
    } else {
        echo "Mots de passe correct !";

        $_SESSION['mail'] = $mail; $_SESSION['password'] = $password;
        $_SESSION['prenom'] = $data['prenom']; $_SESSION['nom'] = $data['nom']; $_SESSION['ville'] = $data['ville'];
        $_SESSION['permission'] = $data['permission']; $_SESSION['numero'] = $data['numero']; $_SESSION['naissance'] = $data['naissance'];
        $_SESSION['domicile'] = $data['adresse_domicile']; $_SESSION['abonnement'] = $data['abonnement'];

        $perm = $data['permission'];
        if ($perm == 0) {
           header("Location: ./index.php");
        } else if ($perm == 1) {
           header("Location: ./panel_admin.php");
        }
    
        die();
    }
  }

  ?>