<?php 
	session_start();

	if (!isset($_SESSION['permission']) || $_SESSION['permission'] != 1) {
		echo "Vous n'avez pas la permission d'accéder à cette page !";
		die();
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css" />
	<link rel="stylesheet" href="../css/style2.css" />
</head>
<body>

	<!-- Section livre  !-->
	<h1>Livres</h1>
	
	<a href="./books.php">Afficher tous les livres</a>
	
	<h2>Supprimer</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="post">
			<br>
			<select name="book-remove" class="input-1 form-control" size="5">
			<?php 
				$sql = new mysqli("localhost", "root", "", "librairie");
				$result = $sql -> query("SELECT * FROM livre");

				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['titre'] ." </option>";
				}
			
			?>
			</select>
			<br/>
			<button class="btn btn-danger btn-pad" type="submit">Supprimer</button>
		</form>
	</div>
	
		
	<h2>Ajouter un livre</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="POST">
			<select class="input-2 form-control" name="categorie" size="1">
				<?php 
				$result = $sql -> query("SELECT * FROM livre_categorie");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['nom'] ." </option>";
				}
			?>
			</select><input class="input-2 form-control" type="text" name="ba-titre" placeholder="Titre du livre" required>
			<br/>
			<input class="input-2 form-control" type="text" name="auteur" placeholder="Auteur" required><input class="input-2 form-control" type="text" name="amount" placeholder="Quantité disponible" required>
			<br/>
			<input class="input-2 form-control" type="text" name="resume" placeholder="Résumé du livre" required><input class="input-2 form-control"  type="text" name="photo" placeholder="Image du livre" required>
			<br/>
			<input class="btn btn-danger btn-pad" type="submit" name="Ajouter">
		</form>
	</div>

	<h2>Modification d'un livre</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="POST">
			<select class="input-1 form-control" name="titre" size="1">
			<?php 
				$result = $sql -> query("SELECT * FROM livre");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['titre'] ." </option>";
				}
			?>
			</select>
			<input class="input-2 form-control" type="text" name="bm-auteur" placeholder="Auteur" required><input class="input-2 form-control" type="text" name="amount" placeholder="Quantité" required>
			<br>
			<input class="input-2 form-control" type="text" name="resume" placeholder="Résumé" required><input class="input-2 form-control" type="text" name="photo" placeholder="Lien photo" required>
			<br/>
			<input class="btn btn-danger btn-pad" type="submit" name="Valider">
		</form>
	</div>
	
	<h2>Prêter un livre</h2>
	<div class="cont-bloc">
		<?php
		include './book_loan.php'; 
		?>
	</div>

	<!-- Section Adhérent  !-->
	<h1>Adhérents</h1>

	<h2>Liste des adhérents</h2>
	<div class="cont-bloc form-group">
		<select class="input-1 form-control" name="titre" size="10">
			<?php 
				$result = $sql -> query("SELECT * FROM user");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['adresse_mail'] .  " - " . $row['nom'] . " " . $row['prenom'] . " (" . ($row['abonnement'] == null || $row['abonnement'] == "0000-00-00" ? "Aucun abonnement" : "Fin abonnement " . $row['abonnement'] . "" ). ") </option>";
				}
			?>
		</select>
	</div>
		
	<h2>Ajouter un adhérent</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="POST">
			<input class="input-2 form-control" type="mail" name="usrmail" placeholder="Adresse mail" required><input class="input-2 form-control" type="password" name="usrpassword" placeholder="Mots de passe" required>
			<br/>
			<input class="input-2 form-control" type="text" name="usrsurname" placeholder="Nom" required><input class="input-2 form-control" type="text" name="usrname" placeholder="Prénom" required>
			<br/>
			<input class="input-2 form-control" type="date" name="usrbirthday" placeholder="Date de naissance" required><input class="input-2 form-control" type="tel" name="usrtel" placeholder="Téléphone portable" required>
			<br/>
			<input class="input-2 form-control" type="text" name="usrdom" placeholder="Domicile" required><input class="input-2 form-control" type="text" name="usrcity" placeholder="Ville" required>
			<br/>
			<button class="btn btn-danger btn-pad" type="submit">Valider l'inscription</button>
		</form>
	</div>
		
	<h2>Supprimer un adhérent</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="post">
			<br>
			<select class="input-1 form-control" name="user-remove" size="5">
			<?php
				$result = $sql -> query("SELECT * FROM user");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['adresse_mail'] . "</option>";
				}
			?>
			</select>
			<br/>
			<button class="btn btn-danger btn-pad" type="submit">Supprimer</button>
		</form>
	</div>
		
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="POST">
		
			<select class="input-1 form-control" name="mail" size="1">
			<?php
				$result = $sql -> query("SELECT * FROM user");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['adresse_mail'] ." </option>";
				}
			?>
			</select>
			<br>
			<input class="input-1 form-control" type="text" name="user-modif" placeholder="Mots de passe" required>
			<br>
			<input class="input-2 form-control" type="text" name="usrsurname" placeholder="Nom" required><input class="input-2 form-control" type="text" name="usrname" placeholder="Prénom" required>
			<br>
			<input class="input-2 form-control" type="date" name="usrbirthday" placeholder="Date de naissance" required><input class="input-2 form-control" type="tel" name="usrtel" placeholder="Téléphone portable" required>
			<br>
			<input class="input-2 form-control" type="text" name="usrdom" placeholder="Adresse de domicile" required><input class="input-2 form-control" type="text" name="usrcity" placeholder="Ville" required>
			<br>
			<input class="btn btn-danger btn-pad" type="submit" name="Valider">
		</form>
	</div>
	
	<div class="cont-bloc">
		<form class="form-group" accept="./panel_admin.php" method="POST">
			<select class="input-1 form-control" name="mail" size="1">
			<?php 
				$result = $sql -> query("SELECT * FROM user");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['adresse_mail'] ." </option>";
				}
			?>
			</select>
			<input class="input-1 form-control" type="date" name="user-abo" placeholder="Echeance d'abonnement">
			<select class="input-1 form-control" name="state" size="1">
			<option>Modifier</option>
			<option>Supprimer</option>
			</select>
			<br>
			<input class="btn btn-danger btn-pad" type="submit" name="Valider">
		</form>
	</div>
</body>
</html>

<?php

	if (isset($_POST['book-remove'])) {
		$title = $_POST['book-remove'];

   		$result = $sql -> query("DELETE FROM livre WHERE titre = '$title'");
   		if ($result) {
   			echo "<meta http-equiv='refresh' content='0'>";
   		} else {
   			echo "Une erreur est survenue !";
   		}
	} else if (isset($_POST['ba-titre'])) {
		$title = $_POST['ba-titre']; $author = $_POST['auteur']; $amount = $_POST['amount'];
		$desc = $_POST['resume']; $photo = $_POST['photo']; $categorie = $_POST['categorie'];

		$result = $sql -> query("SELECT * FROM livre WHERE titre = '$title'");

		if (mysqli_num_rows($result) > 0) {
   			echo "Un livre ayant ce titre existe déjà !";
   			exit;
		}

		$result = $sql -> query("
			INSERT INTO livre(auteur, categorie, id, photo_link, quantite, resume, titre) 
			VALUES('$author', '$categorie', '', '$photo', '$amount', '$desc', '$title')
			");	

   		if ($result) {
   			echo "<meta http-equiv='refresh' content='0'>";
   		} else {
   			echo "Une erreur est survenue !";
   		}
	} else if (isset($_POST['bm-auteur'])) {
		$title = $_POST['titre']; $author = $_POST['bm-auteur']; $amount = $_POST['amount'];
		$desc = $_POST['resume']; $photo = $_POST['photo'];

		$result = $sql -> query("UPDATE livre SET auteur='$author', quantite='$amount', resume='$desc', photo_link='$photo' WHERE titre = '" . $title . "'");	
		echo "<meta http-equiv='refresh' content='0'>";
	} else if (isset($_POST['usrmail'])) {
		$mail = $_POST['usrmail']; $password = $_POST['usrpassword']; 
		$nom = $_POST['usrsurname']; $prenom = $_POST['usrname'];
		$birthday = $_POST['usrbirthday']; $tel = $_POST['usrtel']; $adresse = $_POST['usrdom'];
		$city = $_POST['usrcity'];	

		$result = $sql -> query("SELECT * FROM user WHERE adresse_mail = '$mail'");

		if (mysqli_num_rows($result) > 0) {
   			echo "Un compte ayant cette adresse mail existe déjà !";
   			exit;
		}
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			echo "Adresse mail est incorrect !";
			exit;
		}

		$result = $sql -> query("
			INSERT INTO user(password, nom, prenom, numero, adresse_domicile, naissance, ville, adresse_mail) 
			VALUES('$password', '$nom', '$prenom', '$tel', '$adresse', '$birthday', '$city', '$mail')
			");	

		if ($result) {
   			echo "<meta http-equiv='refresh' content='0'>";
		} else {
			echo "Une erreur est survenue lors de la création de votre compte !";
		}
	
	} else if (isset($_POST['user-remove'])) {
		$mail = $_POST['user-remove'];

   		$result = $sql -> query("DELETE FROM user WHERE adresse_mail = '$mail'");
   		if ($result) {
   			echo "<meta http-equiv='refresh' content='0'>";
   		} else {
   			echo "Une erreur est survenue !";
   		}
	} else if (isset($_POST['user-modif'])) {
		$password = $_POST['user-modif']; 
		$nom = $_POST['usrsurname']; $prenom = $_POST['usrname'];
		$birthday = $_POST['usrbirthday']; $tel = $_POST['usrtel']; $adresse = $_POST['usrdom'];
		$city = $_POST['usrcity'];	
		
		$result = $sql -> query("UPDATE user SET password='$password', nom='$nom', prenom='$prenom', numero='$tel', 
			adresse_domicile='$adresse', naissance='$birthday', ville='$city' WHERE adresse_mail = '" . $_POST['mail'] . "'");	

		echo "<meta http-equiv='refresh' content='0'>";
	} else if (isset($_POST['user-abo'])) {
		$mail = $_POST['mail'];
		$state = $_POST['state'];

		if ($state == "Oui") {
			$result = $sql -> query("UPDATE user SET abonnement='NULL' WHERE adresse_mail = '" . $mail . "'");	
		} else if (isset($_POST['user-abo'])){
			$date = $_POST['user-abo'];
			$result = $sql -> query("UPDATE user SET abonnement='$date' WHERE adresse_mail = '" . $mail . "'");	
		}

		echo "<meta http-equiv='refresh' content='0'>";
	}



?>
