
<?php
	session_start();
?>


<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/style.css" />
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet"> 
	
	
</head>
<body>
	
	<div class="labform">
	<h1>Votre Compte</h1>
	
	<form action="./profile.php" method="POST">
	<div class="lab">
		<label class="lab"><b>Mots de passe</b></label>
		<br>
		<label class="lab"><b>Nom</b></label>
		<br>
		<label class="lab"><b>Prénom</b></label>
		<br>
		<label class="lab"><b>Numéro de téléphone</b></label>
		<br>
		<label class="lab"><b>Adresse du domicile</b></label>
		<br>
		<label class="lab"><b>Ville</b></label>
		<br>
		<label class="lab"><b>Date de naissance</b></label>
	</div>
	<div class="form">
		<input class="formu" type="password" placeholder="Entrer votre mot passe" name="usrpsw" value="<?php echo $_SESSION['password']; ?>">
		<br>
		<input class="formu" type="text" name="usrsurname" placeholder="Enter votre nom" value="<?php echo $_SESSION['nom']; ?>">
		<br>
		<input class="formu" type="text" name="usrname" placeholder="Enter votre prénom" value="<?php echo $_SESSION['prenom']; ?>">
		<br>
		<input class="formu" type="tel" name="usrtel" placeholder="Enter votre numéro de téléphone portable" value="<?php echo $_SESSION['numero']; ?>">
		<br>
		<input class="formu" type="text" name="usrdom" placeholder="Enter votre adresse de domicile" value="<?php echo $_SESSION['domicile']; ?>">
		<br>
		<input class="formu" type="text" name="usrcity" value="<?php echo $_SESSION['ville']; ?>">
		<br>
		<input class="formu" type="date" name="usrbirthday" value="<?php echo $_SESSION['naissance']; ?>">
		
		
	</div>
	<button type="submit" class="val">Enregistrer les modifications</button>
	</form>
	</div>
	
</body>
</html>

<?php

	if (!isset($_SESSION['mail'])) {
		header("Location: ./index.php");
        die();
	}
	if (isset($_POST['usrpsw'])) {
		
		$password = $_POST['usrpsw']; 
		$nom = $_POST['usrsurname']; $prenom = $_POST['usrname'];
		$birthday = $_POST['usrbirthday']; $tel = $_POST['usrtel']; $adresse = $_POST['usrdom'];
		$city = $_POST['usrcity'];	

		// check si l'utilisateur a bien remplis tous les champs
		$data = array($password, $nom, $prenom, $birthday, $tel, $adresse, $city);
		foreach ($data as $key => $val) {
			if (empty($val)) {
				echo "Vérifier que tous les champs soit remplis correctement !";
				exit;
			}
		}

		$sql = new mysqli("localhost", "root", "", "librairie");
		
		$result = $sql -> query("UPDATE user SET password='$password', nom='$nom', prenom='$prenom', numero='$tel', 
			adresse_domicile='$adresse', naissance='$birthday', ville='$city' WHERE adresse_mail = '" . $_SESSION['mail'] . "'");	

		if ($result) {
			$_SESSION['password'] = $password;
      		$_SESSION['prenom'] = $prenom; $_SESSION['nom'] = $nom; $_SESSION['ville'] = $city;
            $_SESSION['numero'] = $tel; $_SESSION['naissance'] = $birthday;
        	$_SESSION['domicile'] = $adresse;

			header("Location: ./index.php");
			die();
		} else {
			echo "Une erreur est survenue lors de la modification de votre compte !";
		}
	}
?>