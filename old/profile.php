
<?php
	session_start();
?>


<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" />
	<link rel="stylesheet" href="css/style.css" />
	
	
	
</head>
<body>
	
	<div class="labform">
	<h1>Votre Compte</h1>
	
	<form class="form-group" action="./profile.php" method="POST">
	<div class="lab">
		<label class="lab"><b>Mail</b></label>
		<br>
		<label class="lab"><b>Mot de passe</b></label>
		<br>
		<label class="lab"><b>Nom</b></label>
		<br>
		<label class="lab"><b>Prenom</b></label>
		<br>
		<label class="lab"><b>Adresse</b></label>
		<br>
		<label class="lab"><b>Ville</b></label>
		<br>
		<label class="lab"><b>Date de naissance</b></label>
	</div>
	<div class="form">
		<input class="form-control formu" type="text" name="mail" placeholder="Mail" value="<?php echo $user[USER_LNAME]; ?>">
	
		<input class="form-control formu" type="password" name="password" placeholder="Mot de passe" value="<?php echo $user[USER_PASSWORD]; ?>">
		
		<input class="form-control formu" type="text" name="lname" placeholder="Nom" value="<?php echo $user[USER_LNAME]; ?>">
		
		<input class="form-control formu" type="text" name="fname" placeholder="Prenom" value="<?php echo $user[USER_FNAME]; ?>">
		
		<input class="form-control formu" type="text" name="addr" placeholder="Adresse" value="<?php echo $user[USER_ADDR]; ?>">
		
		<input class="form-control formu" type="text" name="city" placeholder="Ville" value="<?php echo $user[USER_CITY]; ?>">
		
		<input class="form-control formu" type="date" name="bird" placeholder="Date de naissance" value="<?php echo $user[USER_BIRD]; ?>">
	</div>
	<button type="submit" class="btn btn-default">Enregistrer les modifications</button>
	</form>
	</div>
	
</body>
</html>

<?php

	if (!isset($_SESSION['mail'])) {
		header("Location: ./index.php");
        die();
	}
	if (isset($_POST['usrpsw'])) {
		
		$password = $_POST['usrpsw']; 
		$nom = $_POST['usrsurname']; $prenom = $_POST['usrname'];
		$birthday = $_POST['usrbirthday']; $tel = $_POST['usrtel']; $adresse = $_POST['usrdom'];
		$city = $_POST['usrcity'];	

		// check si l'utilisateur a bien remplis tous les champs
		$data = array($password, $nom, $prenom, $birthday, $tel, $adresse, $city);
		foreach ($data as $key => $val) {
			if (empty($val)) {
				echo "Vérifier que tous les champs soit remplis correctement !";
				exit;
			}
		}

		$sql = new mysqli("localhost", "root", "", "librairie");
		
		$result = $sql -> query("UPDATE user SET password='$password', nom='$nom', prenom='$prenom', numero='$tel', 
			adresse_domicile='$adresse', naissance='$birthday', ville='$city' WHERE adresse_mail = '" . $_SESSION['mail'] . "'");	

		if ($result) {
			$_SESSION['password'] = $password;
      		$_SESSION['prenom'] = $prenom; $_SESSION['nom'] = $nom; $_SESSION['ville'] = $city;
            $_SESSION['numero'] = $tel; $_SESSION['naissance'] = $birthday;
        	$_SESSION['domicile'] = $adresse;

			header("Location: ./index.php");
			die();
		} else {
			echo "Une erreur est survenue lors de la modification de votre compte !";
		}
	}
?>