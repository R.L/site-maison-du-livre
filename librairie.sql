-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 11 juin 2019 à 21:18
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `librairie`
--

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

DROP TABLE IF EXISTS `emprunt`;
CREATE TABLE IF NOT EXISTS `emprunt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `livre_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `date_fin` date NOT NULL,
  `last_mail` date NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '1',
  `relance` int(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_livre_id` (`livre_id`),
  KEY `FK_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

DROP TABLE IF EXISTS `livre`;
CREATE TABLE IF NOT EXISTS `livre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(32) DEFAULT NULL,
  `auteur` varchar(32) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `quantite` int(255) NOT NULL,
  `resume` text NOT NULL,
  `photo64` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(16) NOT NULL,
  `nom` varchar(16) NOT NULL,
  `prenom` varchar(16) NOT NULL,
  `adresse_domicile` varchar(255) NOT NULL,
  `naissance` date NOT NULL COMMENT 'date de naissance',
  `ville` varchar(32) NOT NULL,
  `abonnement` date DEFAULT NULL COMMENT 'date d''abonnement',
  `permission` int(1) NOT NULL DEFAULT '0',
  `adresse_mail` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `password`, `nom`, `prenom`, `adresse_domicile`, `naissance`, `ville`, `abonnement`, `permission`, `adresse_mail`) VALUES
(10, 'aze', 'aze', 'zae', 'aze', '2017-11-24', 'zae', '2017-11-14', 1, 'zezea@gmail.com'),
(11, 'fdp', 'AnisS', 'Jesu', 'eE', '2017-11-23', 'Bagdad', NULL, 1, 'jesus@gmail.com'),
(12, 'htreert', 'rthjkyrt', 'tyjk', 'rtrtyui', '2017-12-06', 'ertyukk', NULL, 0, 'e@e.e'),
(13, 'mdp', 'admin', 'admin', '666', '2017-12-21', '666', '2017-12-30', 1, '666'),
(14, 'mdp', 'admin', 'admin', '666', '2017-12-21', '666', '2017-12-30', 1, '666');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `FK_livre_id` FOREIGN KEY (`livre_id`) REFERENCES `livre` (`id`),
  ADD CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
