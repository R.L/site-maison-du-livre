<?php

function getBookBySearch(string $search)
{
    $search = preg_replace('/[^A-Za-z0-9-]/', ' ', $search);

    // DETACHE LES MOTS CLES ET LES ASSEMBLE POUR LA QUERY
    $echarray = explode(" ", $search); // DETACHAGE
    $qere = "'";
    $qere = $qere . $echarray['0'];
    for ($it = 1; $it < sizeof($echarray); $it ++) {
        $qere = $qere . "|" . $echarray[$it];
    }
    $qere = $qere . "'";

    // QUERY utiliser REGEXP
    $result = DoRequest("SELECT * FROM livre WHERE titre REGEXP " . $qere . " OR resume REGEXP " . $qere,array());

    // ATTRIBUTION DE POINT PAR NOMBRE D'OCCURENCE ET RANGEMENT DU TABLEAU D'UN LIVRE EN FONCTION DU SCORE
    $ctab = array();
    foreach($result as $index=> $line) {
        $score = 0;
        for ($it2 = 0; $it2 < sizeof($echarray); $it2 ++) {
            $score = $score + substr_count($line['titre'], $echarray[$it2]);
            $score = $score + substr_count($line['resume'], $echarray[$it2]);
        }
        $ctab[$score][] = $line;
    }
    // (naturellement le taleau est trié de la clé la plus petite a la plus grande -> besoin d'inverser)
    $ctab = array_reverse($ctab, true);
}

function getBooks(){
    $request= "SELECT * FROM " . LIVRE;
    $array= array();
    return DoRequest($request,$array);
}

function getBookById($id){
    $request= "SELECT * FROM " . LIVRE . " WHERE " . LIVRE_ID. "=?";
    $array= [$id];
    return DoRequest($request,$array);
}

?>