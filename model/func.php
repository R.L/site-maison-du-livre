<?php

/**
 * verifie si une variable d'un tableau existe et est differente de null 
 * @param array() $dollar
 * @param string $name
 * @return boolean
 */
function SubCheck($dollar, $name)
{
    return (isset($dollar[$name]) && $dollar[$name]!=null);
}

/**
 * teste si toutes les variables de l'array existent </br>
 * (par exemple dans un $_POST/$_GET)
 *
 * @param array $dollar
 * @param array(string) $arrayname
 * @return boolean
 */
function SubChecks($dollar, $arrayname)
{
    foreach ($arrayname as $name) {
        if (!isset($dollar[$name]) || !$dollar[$name]!=null) {
            return false;
        }
    }
    return true;
}

/**
 *
 * @param mixed $var
 * @return boolean
 */
function VarCheck($var)
{
    return (isset($var)&&$var!=null);
}

/**
 * verifie une variable de $_POST
 *
 * @param string $name
 * @return boolean
 */
function PostCheck($name)
{
    return SubCheck($_POST, $name);
}

/**
 * verifie une variable de $_GET
 *
 * @param string $name
 * @return boolean
 */
function GetCheck($name)
{
    return SubCheck($_GET, $name);
}

/**
 * verifie une variable de $_SESSION
 *
 * @param string $name
 * @return boolean
 */
function SessionCheck($name)
{
    return SubCheck($_SESSION, $name);
}

/**
 * verifie une variable de $_SERVER
 *
 * @param string $name
 * @return boolean
 */
function ServerCheck($name)
{
    return SubCheck($_SERVER, $name);
}

/**
 * verifie une variable de $_COOKIE
 *
 * @param string $name
 * @return boolean
 */
function CookieCheck($name)
{
    return SubCheck($_COOKIE, $name);
}

/**
 * demarre la session
 */
function StartSession()
{
    if (session_status()==PHP_SESSION_NONE) {
        session_start();
    }
}

/**
 * termine la session
 */
function EndSession()
{
    StartSession();
    session_destroy();
}

/**
 * recupère les informations de connexion
 *
 * @return array('mail'=>$mail,'mdp'=>$mdp) ou null
 */
function GetLoginInfo()
{
    StartSession();
    if (SessionCheck('mail')&&SessionCheck('mdp')) {
        return array(
            'mail' => $_SESSION['mail'],
            'mdp' => $_SESSION['mdp']
        );
    }
    return null;
}

/**
 * set les info de connexion
 *
 * @param string $mail
 * @param string $mdp
 */
function SetLoginInfo($mail, $mdp)
{
    StartSession();
    $_SESSION['mail']= $mail;
    $_SESSION['mdp']= $mdp;
}

/**
 *
 * récupère les informations liés a l'utilisateur<br/>
 * Exemple: $user=GetUser();<br/>
 * $user[USER_NAME] (nom), $user[USER_PASSWORD] (mdp)
 *
 * @return array() ou null
 */
function GetSessionUser()
{
    $coinfo= GetLoginInfo();
    if ($coinfo!=null) {
        $user= GetUserByAuth($coinfo['mail'], $coinfo['mdp']);
        if ($user!=null) {
            return $user;
        } else {
            return null;
        }
    }
    return null;
}

/**
 *
 * @param string $mail
 * @param string $mdp
 * @return number 0=reussite
 *         1=echec
 */
function LogIn($mail, $mdp)
{
    $rtn=GetUserByAuth($mail, $mdp);
    if (isset($rtn)&&$rtn!=null) {
        SetLoginInfo($mail,$mdp);
        return 0;
    } else {
        return 1;
    }
}

/**
 *
 * @param string $mail
 * @param string $mdp
 * @return number 0=reussite
 *         1=mail deja utilisé
 *         2=autre
 */
function Register($mail, $mdp, $fname, $lname, $addr, $bird, $city, $sub, $perm)
{
    if (!CheckAccount($mail)) {
        $rtn= AddUser($mail, $mdp, $fname, $lname, $addr, $bird, $city, $sub, $perm);
        if ($rtn==0) { 
            $lin=GetUserByAuth($mail, $mdp);
            SetLoginInfo($lin[USER_MAIL],$lin[USER_PASSWORD]);
            return 0;  
        }
        return 2;
    }else{
        return 1;
    }
}









