<?php

function AddUser($mail, $mdp, $fname, $lname, $addr, $bird, $city, $sub, $perm)
{
    // prepare requete dans un tab
    $tobuild = array(
        USER_MDP,
        USER_FNAME,
        USER_LNAME,
        USER_ADDR,
        USER_BIRD,
        USER_CITY,
        USER_SUB,
        USER_PERM,
        USER_MAIL
    );
    $req = "INSERT INTO " . USER . BuildInsert1($tobuild) . BuildInsert2($tobuild);
    $param = array(
        $mdp,
        $fname,
        $lname,
        $addr,
        $bird,
        $city,
        $sub,
        $perm,
        $mail
    );
    $rtn = DoRequest($req, $param);
    if ($rtn != 0) {
        return 0;
    } else {
        return 2;
    }
}

function EditUser($id, $mail, $mdp, $fname, $lname, $addr, $bird, $city, $sub, $perm)
{
    $tobuild = array(
        USER_MDP,
        USER_FNAME,
        USER_LNAME,
        USER_ADDR,
        USER_BIRD,
        USER_CITY,
        USER_SUB,
        USER_PERM,
        USER_MAIL
    );
    $request = "UPDATE " . USER . BuildUpdate($tobuild) . "  WHERE " . USER_ID . " = ?";
    $param = array(
        $mdp,
        $fname,
        $lname,
        $addr,
        $bird,
        $city,
        $sub,
        $perm,
        $mail,
        $id
    );
    $rtn = DoRequest($request, $param);
    if ($rtn == 1) {
        return 0;
    } else if ($rtn == 0) {
        return 1;
    } else {
        return 2;
    }
}

function GetUsers()
{
    $req = "SELECT * FROM " . USER;
    $param = array();
    return DoRequest($req, $param);
}

function GetUser($id)
{
    $req = "SELECT * FROM " . USER . " WHERE " . USER_ID . "=? ";
    $param = array(
        $id
    );
    return DoRequest($req, $param)[0];
}

function GetUserByMail($mail)
{
    $req = "SELECT * FROM " . USER . " WHERE " . USER_MAIL . "=? ";
    $param = array(
        $mail
    );
    return DoRequest($req, $param)[0];
}

function GetUserByAuth($mail, $mdp)
{
    $req = "SELECT * FROM " . USER . " WHERE " . USER_MAIL . "=? AND " . USER_PASSWORD . "=? ";
    $param = array(
        $mail,
        $mdp
    );
    return DoRequest($req, $param)[0];
}

function DeleteUser($id_usr)
{
    $req = "DELETE FROM " . USER . " WHERE " . USER_ID . "=?";
    $param = array(
        $id_usr
    );

    return DoRequest($req, $param)[0];
    // var_dump($req);
}


