<?php

/**
 * pour afficher vos erreurs 
 * @param string $str
 */
function PrintError($str)
{
    echo ('<strong>ERREUR:</strong> ' . $str);
}

/**
 * recupere la base de donnee
 *
 * @return PDO
 */
function getDataBase()
{
    try {
        $dbstr = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';port=' . DB_PORT . ';charset=utf8';
        $bdd = new PDO($dbstr, DB_USER, DB_PASSWORD, array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ));
    } catch (Exception $e) {
        $bdd = null;
        die('Erreur : ' . $e->getMessage());
    }
    return $bdd;
}

/**
 *
 * Exemple: <br/>
 * DoRequest("SELECT * FROM table WHERE id_table=:id",array(":id"=>$id)); <br/> <br/>
 * utilisez les constantes BDD pour acceder au champs du tableau retourne <br/>
 * Exemple: $user_mail_0=$results[0][USER_MAIL];
 *
 * @param $request string
 * @param $params array(string=>string)
 * @return array(array()) ou null
 */
function DoRequest($request, $params)
{
    $bdd = getDataBase();
    $stmt = $bdd->prepare($request);
    $result = $stmt->execute($params);
    if (strpos($request, 'SELECT') !== false) {
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    $stmt->closeCursor();
    return $result;
}

/**
 * verifie que le mail et mdp corresponde,<br/>
 *
 * @param string $mail
 * @param string $psw
 * @return boolean si user existe
 */
function CheckAccount($mail)
{
    $req = "SELECT COUNT(*) count FROM " . USER . " WHERE " . USER_MAIL . "=?";
    $param = array(
        $mail
    );
    $rtn = DoRequest($req, $param)[0]['count'];
    return ($rtn == 1);
}

function BuildInsert1(array $arr)
{
    $rtn = "(";
    foreach ($arr as $index => $item) {
        if ($index != 0) {
            $rtn = $rtn . ",";
        }
        $rtn = $rtn . $item;
    }
    $rtn = $rtn . ")";
    return $rtn;
}

function BuildInsert2(array $arr)
{
    $rtn = " VALUE(";
    foreach ($arr as $index => $item) {
        if ($index != 0) {
            $rtn = $rtn . ",";
        }
        $rtn = $rtn . "?";
    }
    $rtn = $rtn . ")";
    return $rtn;
}

function BuildUpdate(array $arr)
{
    $rtn = " SET ";
    foreach ($arr as $index => $item) {
        if ($index != 0) {
            $rtn = $rtn . ",";
        }
        $rtn = $rtn . $item . "=?";
    }
    $rtn = $rtn . " ";
    return $rtn;
}







