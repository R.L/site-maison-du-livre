<?php
//connexion a la bdd
const DB="localhost";
const DB_NAME="librairie";
const DB_USERNAME="root";
const DB_PASSWORD="";
const DB_PORT="";

//table livre
const LIVRE="livre";
const LIVRE_ID="id";
const LIVRE_TITLE="titre";
const LIVRE_AUTHOR="auteur";
const LIVRE_TYPE="categorie";
const LIVRE_STOCK="quantite";
const LIVRE_RESUME="resume";
const LIVRE_PHOTO="photo64";

//table utilisateur
const USER="user";
const USER_ID="id";
const USER_MDP="password";
const USER_FNAME="nom";
const USER_LNAME="prenom";
const USER_ADDR="adresse_domicile";
const USER_BIRD="naissance";
const USER_CITY="ville";
const USER_SUB="abonnement";
const USER_PERM="permission";
const USER_MAIL="adresse_mail";

//table emprunt
const EMPR="emprunt";
const EMPR_ID="id";
const EMPR_BOOK="livre_id";
const EMPR_USER="user_id";
const EMPR_FIN="date_fin";
const EMPR_LASTMAIL="last_mail";
const EMPR_ACT="actif";
const EMPR_RELANCE="relance";













