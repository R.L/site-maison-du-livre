<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
	<meta charset="utf-8">
</head>
<body>
	<h2>Supprimer un adhérent</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="post">
			<br>
			<select class="input-1 form-control" name="user-remove" size="5">
			<?php
				$result = $sql -> query("SELECT * FROM user");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['adresse_mail'] . "</option>";
				}
			?>
			</select>
			<br/>
			<button class="btn btn-danger btn-pad" type="submit">Supprimer</button>
		</form>
	</div>
</body>
</html>

<?php 
	if (isset($_POST['user-remove'])) {
		$mail = $_POST['user-remove'];

   		$result = $sql -> query("DELETE FROM user WHERE adresse_mail = '$mail'");
   		if ($result) {
   			echo "<meta http-equiv='refresh' content='0'>";
   		} else {
   			echo "Une erreur est survenue !";
   		}
	} 
?>