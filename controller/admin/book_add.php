<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
	<meta charset="utf-8">
</head>
<body>
	<h2>Ajouter un livre</h2>
	<div class="cont-bloc">
		<form class="form-group" action="./panel_admin.php" method="POST">
			<select class="input-2 form-control" name="categorie" size="1">
				<?php 
				$result = $sql -> query("SELECT * FROM livre_categorie");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['nom'] ." </option>";
				}
			?>
			</select><input class="input-2 form-control" type="text" name="ba-titre" placeholder="Titre du livre" required>
			<br/>
			<input class="input-2 form-control" type="text" name="auteur" placeholder="Auteur" required><input class="input-2 form-control" type="text" name="amount" placeholder="Quantité disponible" required>
			<br/>
			<input class="input-2 form-control" type="text" name="resume" placeholder="Résumé du livre" required><input class="input-2 form-control"  type="text" name="photo" placeholder="Image du livre" required>
			<br/>
			<input class="btn btn-danger btn-pad" type="submit" name="Ajouter">
		</form>
	</div>
</body>
</html>

<?php 
	if (isset($_POST['ba-titre'])) {
		$title = $_POST['ba-titre']; $author = $_POST['auteur']; $amount = $_POST['amount'];
		$desc = $_POST['resume']; $photo = $_POST['photo']; $categorie = $_POST['categorie'];

		$result = $sql -> query("SELECT * FROM livre WHERE titre = '$title'");

		if (mysqli_num_rows($result) > 0) {
   			echo "Un livre ayant ce titre existe déjà !";
   			exit;
		}

		$result = $sql -> query("
			INSERT INTO livre(auteur, categorie, id, photo_link, quantite, resume, titre) 
			VALUES('$author', '$categorie', '', '$photo', '$amount', '$desc', '$title')
			");	

   		if ($result) {
   			echo "<meta http-equiv='refresh' content='0'>";
   		} else {
   			echo "Une erreur est survenue !";
   		}
	}
?>