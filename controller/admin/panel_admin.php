<?php 
	require_once("controller/main.php");
	$user=GetSessionUser();
	if(!isset($user)){
	    
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Bibliothéque de la ville d'Orange</title>
    <link rel="stylesheet" type="text/css" href="panel.css">
	<link href="https://fonts.google.com/specimen/Josefin+Sans" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="../css/style2.css" />
	<meta charset="utf-8">
</head>
<body>
	
	<!-- Section livre  !-->
	<div class="book">
	
	<h1>Livre</h1>


	<?php 
	 	require ('book_remove.php'); 
	 	require ('book_add.php'); 
	 	require ('controller/admin/book_modify.php'); 
	 	require ('book_loan.php'); 
	 	require ('user_add.php'); 
	 	require ('user_remove.php');
	 	require ('user_change.php'); 
	 	require ('user_abo.php'); 
	?>
	</div>
	
	<h2>Liste des adhérents</h2>
	<div class="cont-bloc form-group">
		<select class="input-1 form-control" name="titre" size="10">
			<?php 
				$result = $sql -> query("SELECT * FROM user");
				while ($row = $result -> fetch_assoc()) {
					echo "<option> ". $row['adresse_mail'] .  " - " . $row['nom'] . " " . $row['prenom'] . " (" . ($row['abonnement'] == null || $row['abonnement'] == "0000-00-00" ? "Aucun abonnement" : "Fin abonnement " . $row['abonnement'] . "" ). ") </option>";
				}
			?>
		</select>
	</div>
</body>
</html>